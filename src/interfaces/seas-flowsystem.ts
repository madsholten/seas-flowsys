import { Request } from "express";

export interface ISeasFlowSystem {
    req: Request;
    type: string;
    label?: string;
    ports?: ISeasFlowPort[];
}

export interface ISeasFlowPort {
    label?: string;
    direction?: string;
}