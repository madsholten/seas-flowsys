import { Request } from "express";

export interface ISeasProperty {
    req: Request;
    resourceURI: string;
    property?: string;
    value?: any;
}