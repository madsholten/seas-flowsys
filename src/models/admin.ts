import { Request } from "express";
import * as rp from "request-promise";
import { StardogConn } from "./../config/stardog-connection";
const exec = require('child_process').exec;
var errors = require('request-promise/errors');

//Models
import { BaseModel } from "./model";

//Helper functions
import { UriFunctions } from "./../helpers/uri-functions";

//Config
import { DbConfig } from './../config/database';
import { AppConfig } from './../config/app';

export class AdminModel extends BaseModel {
    //List projects
    listDBs(req: Request){
        return rp({
            uri: `${DbConfig.stardog.host}:${DbConfig.stardog.port}/admin/databases`,
            auth: {
                username: DbConfig.stardog.username,
                password: DbConfig.stardog.password
            }
        });
    }
    //Wipe database
    wipeDB(req: Request){
        //Define constants
        const db: string = req.params.db;
        return this.wipeAll(db);
    }
    //Attach ontology
    attachOntology(req: Request){
        //Define constants
        const db: string = req.params.db;
        const named_graph_name: string = req.body.named_graph_name;
        const graph_url: string = req.body.graph_url;
        
        //Attach external ontology
        return this.loadTTL(db,graph_url,named_graph_name)
            .then(function(data){
                if(data.boolean == true){
                    return "Successfully attached graph data";
                } else{
                    errors.error = "An error occured when trying to attach graph data";
                    errors.statusCode = 500;
                    throw errors;
                }
            });
    }
    //Reload ontology
    reloadOntology(req: Request){
        //Define constants
        const db: string = req.params.db;
        const named_graph_name: string = req.body.named_graph_name;
        const graph_url: string = req.body.graph_url;
        
        //Delete named graph
        
        //Check if named graph exists
        return this.checkIfGraphExist(db, named_graph_name)
            .then(data => {
                //Delete named graph
                const cmd: string = `stardog data remove ${DbConfig.stardog.host}:${DbConfig.stardog.port}/${db}  --named-graph tag:/${named_graph_name} -u ${DbConfig.stardog.username} -p ${DbConfig.stardog.password}`;
                console.log(cmd);
                return this.executeCmd(cmd)
            })
            .then(data => {
                console.log(data);
                return this.loadTTL(db,graph_url,named_graph_name)
            }).then(function(data){
                console.log(data);
                if(data.boolean == true){
                    return "Successfully reattached graph data";
                } else{
                    errors.error = "An error occured when trying to attach graph data";
                    errors.statusCode = 500;
                    throw errors;
                }
            });
    }
    //Delete named graph
    detachOntology(req: Request){
        //Define constants
        const db: string = req.params.db;
        const named_graph_name: string = req.body.named_graph_name;
        
        return this.checkIfGraphExist(db, named_graph_name)
            .then(data => {
                const cmd: string = `stardog data remove ${DbConfig.stardog.host}:${DbConfig.stardog.port}/${db}  --named-graph tag:/${named_graph_name} -u ${DbConfig.stardog.username} -p ${DbConfig.stardog.password}`;
                console.log(cmd);
                return this.executeCmd(cmd);
            });
    }
    
    //Add namespace
    addNamespace(req: Request): any{
        //Define constants
        const db: string = req.params.db;
        const prefix: string = req.body.prefix;
        const uri: string = req.body.uri;
        
        //Execute command
        const cmd: string = `stardog namespace add ${DbConfig.stardog.host}:${DbConfig.stardog.port}/${db} --prefix ${prefix} --uri ${uri} -u ${DbConfig.stardog.username} -p ${DbConfig.stardog.password}`;
        return this.executeCmd(cmd);
    }
    //Remove namespace
    removeNamespace(req: Request): any {
        //Define constants
        const db: string = req.params.db;
        const prefix: string = req.body.prefix;
        
        //Execute command
        const cmd: string = `stardog namespace remove ${DbConfig.stardog.host}:${DbConfig.stardog.port}/${db} --prefix ${prefix} -p ${DbConfig.stardog.password}`;
        return this.executeCmd(cmd);
    }
    //List rules
    listRules(req: Request): any {
        //Define constants
        const db: string = req.params.db;
        const q: string =  `PREFIX rule:   <tag:stardog:api:rule:> \
                            SELECT DISTINCT ?URI ?label ?comment ?rule \
                            WHERE { \
                                ?URI a rule:SPARQLRule ; rdfs:label ?label ; rule:content ?rule . \
                                OPTIONAL{ ?URI rdfs:comment ?comment } \
                            }`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options);
    }
    //Delete rule
    deleteRule(req: Request): any {
        //Define constants
        const db: string = req.params.db;
        const URI: string = req.body.uri;
        
        const q: string =  `DELETE WHERE { <${URI}> ?p ?o }`;
        
        return this.checkIfExist(db,URI)
            .then(data => {
                let dbConn = new StardogConn(db,{query:q});
                console.log("Querying database: "+q);
                return rp(dbConn.options)
            })
            .then(data => {
                if(data.boolean == true){
                    return `Successfully deleted rule`
                }else{
                    errors.error = "Could not delete rule";
                    errors.statusCode = 500;
                    throw errors;
                }
            });
    }
    //Add rule
    addRule(req: Request): any {
        //Define constants
        const db: string = req.params.db;
        const label: string = req.body.label;
        const comment: string = req.body.comment; //Optional
        const rule: string = req.body.rule;
        
        //Create a URI for the rule
        let uf = new UriFunctions(req, "Rule");
        var URI = uf.newUri();
        
        const q: string =  `PREFIX rule:<tag:stardog:api:rule:> \
                            INSERT DATA { \
                                <${URI}> a rule:SPARQLRule ; \
                                        rdfs:label "${label}"^^xsd:string ; \
                                        rdfs:comment "${comment}"^^xsd:string ; \
                                        rule:content "${rule}" . \
                            }`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options)
    }
}