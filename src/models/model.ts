import { Request } from "express";
import * as rp from "request-promise";
import { StardogConn } from "./../config/stardog-connection";

var exec = require('child_process').exec;
var errors = require('request-promise/errors');

var _exec = require('child-process-promise').exec;

//Config
import { DbConfig } from './../config/database';
import { AppConfig } from './../config/app';

//Helper functions
import { UriFunctions } from "./../helpers/uri-functions";

//Interfaces
import { IQueryString } from "./../interfaces/qs";
import { ISeasProperty } from "./../interfaces/seas-property";

export class BaseModel {
    
    getData(db){
        const q = "SELECT * WHERE {?s ?p ?o}";
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options);
    }
    
    wipeAll(db){
        const q = "DELETE WHERE {?s ?p ?o}";
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options)
            .then(result => {
                if(result.boolean == true){
                    return `Successfully wiped the database`;
                }else{
                    errors.error = "Could not delete resources";
                    errors.statusCode = 500;
                    throw errors;
                }
            });
    }
    
    checkIfExist(db,URI){
        const q: string = `ASK WHERE {<${URI}> ?p ?o}`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options)
            .then(exist => {
                if(exist.boolean == true){
                    return exist;
                }else{
                    errors.error = "No entity with the specified URI!";
                    errors.statusCode = 404;
                    throw errors;
                }
            });
    }
    
    checkIfGraphExist(db,graph_name){
        const q: string = `ASK WHERE {GRAPH <${graph_name}> {?s ?p ?o}}`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options)
            .then(exist => {
                if(exist.boolean == true){
                    return exist;
                }else{
                    errors.error = "No named graph with that name in the database!";
                    errors.statusCode = 404;
                    throw errors;
                }
            });
    }
    
    deleteEntity(db,URI){
        const q: string = `DELETE WHERE { <${URI}> ?p ?o . }`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options);
    }
    
    //Get all properties of a resource
    getProperties(db,URI){
        //const q: string = `SELECT ?property ?value WHERE { <${URI}> ?property ?value . }`;
        const q: string =  `SELECT ?property ?value { \
                                { <${URI}> ?property ?value } \
                            UNION \
                                { graph ?g { <${URI}> ?property ?value } } \
                            }`;
        let dbConn = new StardogConn(db,{query:q, reasoning: true});
        console.log("Querying database: "+q);
        return rp(dbConn.options);
    }
    
    addObjectProperty(db, resourceURI, propertyURI, objectURI){
        //Check if resource exists
        return this.checkIfExist(db,resourceURI)
            .then(data => {
                //Check if object exists
                return this.checkIfExist(db,objectURI)
            })
            .then(data => {
                //Attach property
                const q: string = `INSERT DATA {<${resourceURI}> <${propertyURI}> <${objectURI}>}`;
                let dbConn = new StardogConn(db,{query:q, reasoning: true});
                console.log("Querying database: "+q);
                return rp(dbConn.options);
            })
            .then(data => {
                if(data.boolean == true){
                    return `Successfully created object property`;
                }else{
                    errors.error = "Could not create resource";
                    errors.statusCode = 500;
                    throw errors;
                }
            });
    }
    
    //List all properties that can be assigned to a resource of a certain type
    getAvailableProperties(db, typeURI){
        const q: string = `SELECT ?uri ?label \
                            WHERE { \
                                GRAPH ?g { \
                                    <${typeURI}> rdfs:subClassOf   [owl:onProperty ?uri] . \
                                    OPTIONAL{ ?uri rdfs:label ?label } . \
                                } \
                            }`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options);
    }
    
    loadTTL(db, file, prefix){
        const q = `LOAD <${file}> INTO GRAPH <${prefix}>`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options);
    }
    
    getPropertyRange(db, URI){
        const q = `SELECT DISTINCT ?range WHERE { GRAPH ?g { <${URI}> rdfs:range ?range } }`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options).then(data => {
            if(data.results.bindings.length >= 0){
                return data.results.bindings[0].range;
            }else {
                errors.error = "Could not find the range of the specified property. Is the ontology loaded in the database?";
                errors.statusCode = 500;
                throw errors;
            }
        });
    }
    
    executeCmd(cmd): any{
        console.log("Executing command: "+cmd);
        return _exec(cmd).then(result => {
            if(result.stderr){
                errors.error = result.stderr;
                errors.statusCode = 500;
                throw errors;
            }else{
                return result.stdout;
            }
        });
    }
    
    getResourcesOfType(req,typeURI){
        //Define constants
        const db: string = req.params.db;
        
        //Query DB
        const q: string = `SELECT ?entity ?label WHERE { ?entity a <${typeURI}> . OPTIONAL { ?entity rdfs:label ?label } . }`;
        console.log(q);
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options);
    }
    
    createNewResource(req,typeURI){
        //Define constants
        const type: string = typeURI.replace('#','/').split('/').pop(-1); //The type is the last part of the type URI
        const db: string = req.params.db;
        const label: string = req.body.label;
        
        //Create a URI for the resource
        let uf = new UriFunctions(req, type);
        var URI = uf.newUri();
        
        //Put it in the DB
        var q: string;
        if(!label){
            q = `INSERT DATA {<${URI}> a <${typeURI}> . }`;
        }else{
            q = `INSERT DATA {<${URI}> a <${typeURI}> ; rdfs:label "${label}"^^xsd:string . }`;
        }
        
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options).then(data => {
            if(data.boolean == true){
                return `${URI}`
            }else{
                errors.error = "Could not create resource";
                errors.statusCode = 500;
                throw errors;
            }
        });
    }
    
    deleteResource(req){
        //Define constants
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const URI: string = `https://${host}${req.originalUrl}`;
        
        //Check if resource exists and delete if true
        return this.checkIfExist(db,URI)
            .then(data => {
                let bm = new BaseModel;
                return bm.deleteEntity(db,URI)
            })
            .then(result => {
                if(result.boolean == true){
                    return `Resource ${URI} successfully deleted`;
                }else{
                    errors.error = "Could not delete resource";
                    errors.statusCode = 500;
                    throw errors;
                }
            });
    }
    
    getResourceProperties(req){
        //Define constants
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const URI: string = `https://${host}${req.originalUrl}`;
        
        //Check if resource exists and delete if true
        return this.checkIfExist(db,URI)
            .then(exist => {
                let bm = new BaseModel;
                return bm.getProperties(db,URI);
            });
    }

}