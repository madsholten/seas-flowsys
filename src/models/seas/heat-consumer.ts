import { Request } from "express";
import * as rp from "request-promise";
import { StardogConn } from "./../../config/stardog-connection";
var errors = require('request-promise/errors');

//Models
import { SeasModel } from "./_seas";

//Helper functions
import { UriFunctions } from "./../../helpers/uri-functions";

//Config
import { DbConfig } from './../../config/database';
import { AppConfig } from './../../config/app';

//Interfaces
import { ISeasProperty } from "./../../interfaces/seas-property";
import { ISeasFlowSystem } from "./../../interfaces/seas-flowsystem";
import { ISeasFlowPort } from "./../../interfaces/seas-flowsystem";

export class HeatConsumerModel extends SeasModel {

    //Create a new Heat Consumer
    createHeatConsumer(req: Request){
        //Define constants
        const label: string = req.body.label;
        const ports: ISeasFlowPort[] = [{label: "Supply port", direction: "In"}, {label: "Return port", direction: "Out"}];
        const flowSys: ISeasFlowSystem = {req: req, label: label, type: "HeatConsumer", ports: ports};

        //Create flow system
        return this.createFlowSystem(flowSys)
            .then(d => {
                return "Successfully created heat consumer!"
            });
    }

    //List all Heat Consumers in the project
    listHeatConsumers(req: Request){
        //Create resource
        const typeURI = "https://w3id.org/seas/HeatConsumer";
        return this.getResourcesOfType(req,typeURI);
    }

    //Delete a heat consumer and its properties
    //NB! SHOULD BE HANDLED IN ANOTHER WAY
    deleteHeatConsumer(req: Request): any{
        //Delete resource
        return this.deleteResource(req);
    }

    //Get all the properties defined for a specific heat consumer
    getHeatConsumerProperties(req: Request): any{
        //If querying for a specific property
        const property = req.query.property;
        if(property){
            switch(property) {
                case "heatOutput":
                    return this.getConsumerHeatOutput(req);
                case "fluidVolumeFlow":
                    return this.getConsumerVolumeFlow(req);
                default:
                    return new Promise ((resolve, reject) => resolve("Unknown property"));
            }
        }

        //Get all defined properties
        return this.getResourceProperties(req);
    }

    //List all the properties of a heat consumers that are available in the ontology
    listHeatConsumerProperties(req: Request): any{
        //Define constants
        const db: string = req.params.db;
        const typeURI = "https://w3id.org/seas/HeatConsumer";
        
        //List properties
        return this.getAvailableProperties(db, typeURI);
    }

    /**
     * PROPERTIES
     * @method getConsumerHeatOutput
     * @method setHeatConsumerProperty
     * @method setConsumerHeatOutput
     */

    //Get the heat outpu
    getConsumerHeatOutput(req: Request){
        //Define constants
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const resourceURI: string = `https://${host}${req.originalUrl.split('?')[0]}`;
        const property = "consumerHeatOutput";

        //Check if resource exists
        return this.checkIfExist(db,resourceURI)
            .then(d => {
                return this.getSeasProperty(db,resourceURI,property)
            })
    }

    getConsumerVolumeFlow(req: Request){
        //Define constants
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const resourceURI: string = `https://${host}${req.originalUrl.split('?')[0]}`;
        const property = "fluidVolumeFlow";
    }

    //Set a specific property of a heat consumer
    setHeatConsumerProperty(req: Request): any{
        //Get property
        const property = req.query.property;
        switch(property) {
            case "subFlowSystemOf":
                return this.makeSubFlowSystemOf(req);
            case "heatOutput":
                return this.setConsumerHeatOutput(req);
            default:
                return new Promise ((resolve, reject) => resolve("Unknown property"));
        }
    }
    
    setConsumerHeatOutput(req: Request){
        //Define constants
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const resourceURI: string = `https://${host}${req.originalUrl.split('?')[0]}`;
        const output: string = req.body.output;
        const sp: ISeasProperty = {req: req, resourceURI: resourceURI };

        //Check if resource exists
        return this.checkIfExist(db,resourceURI)
            .then(data => {
                //Check that input is correctly specified
                if(this.checkCDTUnit(output, "W")){
                    return data;
                } else {
                    errors.error = "Please specify an output effect in Watts. Example: '600 W' See http://unitsofmeasure.org/ucum.html#para-30 for further specifications.";
                    errors.statusCode = 400;
                    throw errors;
                }
            })
            .then(data => {
                let sm = new SeasModel;
                sp.property = "consumerHeatOutput";
                sp.value = output;
                return sm.addSeasPropertyValueSet(sp);
            });
    }
}