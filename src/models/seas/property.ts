import { Request } from "express";
import * as rp from "request-promise";
import { StardogConn } from "./../../config/stardog-connection";
var errors = require('request-promise/errors');

//Models
import { SeasModel } from "./_seas";

//Helper functions
import { UriFunctions } from "./../../helpers/uri-functions";

//Config
import { DbConfig } from './../../config/database';
import { AppConfig } from './../../config/app';

export class PropertyModel extends SeasModel {

    //Get property evaluations
    getProperty(req: Request){
        //Create resource
        const db:string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const resourceURI: string = `https://${host}${req.originalUrl.split('?')[0]}`;

        return this.checkIfExist(db,resourceURI)
            .then(d => {
                return this.getPropertyEvaluations(db,resourceURI);
            })
    }
}