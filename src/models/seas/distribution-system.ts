import { Request } from "express";
import * as rp from "request-promise";
import { StardogConn } from "./../../config/stardog-connection";
var errors = require('request-promise/errors');

//Models
import { SeasModel } from "./_seas";

//Helper functions
import { UriFunctions } from "./../../helpers/uri-functions";

//Config
import { DbConfig } from './../../config/database';
import { AppConfig } from './../../config/app';

//Interfaces
import { ISeasProperty } from "./../../interfaces/seas-property";

export class DistributionSystemModel extends SeasModel {
    
    //Create a new Flow Hub
    createHub(req: Request){
        //Create resource
        const typeURI = "https://w3id.org/seas/FlowHub";
        return this.createNewResource(req,typeURI);
    }

    //Get pipes
    getPipes(req: Request): any {
        const db: string = req.params.db;

        //Create pipe and connections
        const q =  `PREFIX seas: <https://w3id.org/seas/> \
                    SELECT ?pipeURI ?exchangesFluidWith WHERE { \
                        ?pipeURI a seas:Pipe ; \
                            seas:exchangesFluidWith ?exchangesFluidWith . \
                    }`;
        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options).then(data => {
            var list = [];

            for(var i in data.results.bindings){
                list.push({
                    pipeURI: data.results.bindings[i].pipeURI.value,
                    connectsURI: data.results.bindings[i].exchangesFluidWith.value
                });
            }

            return list;
        });
    }

    /**
     * PROPERTIES
     * @method setHubProperty
     * @method createPipeConnection
     */

    setHubProperty(req: Request): any{
        //Get property
        const property = req.query.property;
        switch(property) {
            case "connectTo":
                return this.createPipeConnection(req);
            default:
                return new Promise ((resolve, reject) => resolve("Unknown property"));
        }
    }

    //NB! Needs to be updated to seas:feeds / seas:fedBy
    createPipeConnection(req: Request){
        //Define constants
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const sourceURI: string = `https://${host}${req.originalUrl.split('?')[0]}`;
        const targetURI: string = req.body.target_URI;

        //Create URIs
        var pipeURI = new UriFunctions(req, "Pipe").newUri();
        //var sourcePortURI = new UriFunctions(req, "FlowPort").newUri();
        //var targetPortURI = new UriFunctions(req, "FlowPort").newUri();

        //Check if source exists
        return this.checkIfExist(db,sourceURI)
            .then(data => {
                //Check if target exists
                return this.checkIfExist(db,targetURI);
            })
            .then(data => {
                //Create pipe and connections
                const q =  `PREFIX seas: <https://w3id.org/seas/> \
                            INSERT DATA { \
                                <${pipeURI}> a seas:Pipe ; \
                                    seas:exchangesFluidWith <${sourceURI}> , <${targetURI}> . \
                            }`;
                let dbConn = new StardogConn(db,{query:q});
                console.log("Querying database: "+q);
                return rp(dbConn.options)
            })
            .then(data => {
                if(data.boolean == true){
                    return `Successfully created connection`
                }else{
                    errors.error = "Could not create connection";
                    errors.statusCode = 500;
                    throw errors;
                }
            });
    }
}