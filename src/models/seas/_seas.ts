import { Request } from "express";
import * as rp from "request-promise";
import * as moment from "moment";
import { StardogConn } from "./../../config/stardog-connection";
var errors = require('request-promise/errors');

//Models
import { BaseModel } from "./../model";

//Helper functions
import { UriFunctions } from "./../../helpers/uri-functions";

//Config
import { DbConfig } from './../../config/database';
import { AppConfig } from './../../config/app';

//Interfaces
import { ISeasProperty } from "./../../interfaces/seas-property";
import { ISeasFlowSystem } from "./../../interfaces/seas-flowsystem";

export class SeasModel extends BaseModel {

    /**
     * CREATE FLOW SYSTEM
     */
     createFlowSystem(input: ISeasFlowSystem): any{
        const db: string = input.req.params.db;

        //Create resource URI
        const resourceURI = new UriFunctions(input.req, "HeatConsumer").newUri();
        
        //Create port URIs
        const noPorts: number = input.ports.length;
        var portURIs: string[] = [];

        for(var i in input.ports){
            var URI: string = new UriFunctions(input.req, "FlowPort").newUri();
            portURIs.push(URI);
        }

        var q = `PREFIX seas: <https://w3id.org/seas/> \
                 PREFIX cdt: <http://w3id.org/lindt/custom_datatypes#> \
                 INSERT DATA { \
                        <${resourceURI}> a seas:${input.type} ; `;
        if(input.label){
            q += `rdfs:label "${input.label}"^^xsd:string ; `
        }
        if(input.ports){
            //Add connections between resource and ports
            q += "seas:connectsAt ";
            for(var i in portURIs){
                q += `<${portURIs[i]}>`;
                q += (Number(i) != noPorts-1) ? " , " : " . ";
            }
            //Add data about flow ports if available
            for(var i in portURIs){
                q += `<${portURIs[i]}> a seas:FlowPort ; `;
                if(input.ports[i].direction) {
                    q+= `seas:flowDirection [ seas:value "${input.ports[i].direction}"^^xsd:string ] ; `;
                }
                if(input.ports[i].label) {
                    q+= `rdfs:label "${input.ports[i].label}"^^xsd:string `;
                }
                q += `.`;
            }
            q += ` }`;
        }

        let dbConn = new StardogConn(db,{query:q});
        console.log("Querying database: "+q);
        return rp(dbConn.options)
            .then(data => {
                if(data.boolean == true){
                    return `Successfully created flow system`;
                }else{
                    errors.error = "Could not create heat consumer";
                    errors.statusCode = 500;
                    throw errors;
                }
            });
     }

    makeSubFlowSystemOf(req: Request){
        //Define constants
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const resourceURI: string = `https://${host}${req.originalUrl.split('?')[0]}`;
        const objectURI: string = req.body.super_system_URI;
        const seas: string = "https://w3id.org/seas/";
        const propertyURI: string = seas+"subFlowSystemOf";
        
        return this.addObjectProperty(db,resourceURI,propertyURI,objectURI)
            .then(data => {
                return `Successfully made ${resourceURI} a sub flow system of ${objectURI}`
            });
    }

    /**
     * PROPERTIES
     */

    //Check that the specified CDT unit is correct
    checkCDTUnit(string: string, unit: string){
        return string.split(' ').pop() == unit;
    }

    //Get a SEAS property
    //Returns all evaluations of the property
    getSeasProperty(db,URI,prop){
        const q =  `PREFIX seas: <https://w3id.org/seas/> \
                    PREFIX prov: <http://www.w3.org/ns/prov#> \
                    SELECT ?value ?timestamp WHERE {<${URI}> seas:${prop}/seas:evaluation [ seas:value ?value ; prov:wasGeneratedAtTime ?timestamp ] . } ORDER BY DESC(?t)`;
        console.log("Querying database: "+q);
        let dbConn = new StardogConn(db,{query:q});
        return rp(dbConn.options);
    }

    //Get all seas properties and their evaluations
    getPropertyEvaluations(db,URI){
        const q =  `PREFIX seas: <https://w3id.org/seas/> \
                    PREFIX prov: <http://www.w3.org/ns/prov#> \
                    SELECT ?value ?timestamp WHERE {<${URI}> seas:evaluation [ seas:value ?value ; prov:wasGeneratedAtTime ?timestamp ] . } ORDER BY DESC(?t)`;
        console.log("Querying database: "+q);
        let dbConn = new StardogConn(db,{query:q});
        return rp(dbConn.options);
    }

    //Update a property
    addPropertyEvaluation(db,URI,newVal){
        const dateTime = moment().format();
        //Get latest value of the property
        const q =  `PREFIX seas: <https://w3id.org/seas/> \
                    PREFIX cdt: <http://w3id.org/lindt/custom_datatypes#> \
                    PREFIX prov: <http://www.w3.org/ns/prov#> \
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \
                    SELECT ?v WHERE {<${URI}> seas:evaluation [ seas:value ?v ; prov:wasGeneratedAtTime ?t ] . } ORDER BY DESC(?t) LIMIT 1`;
        console.log("Querying database: "+q);
        let dbConn = new StardogConn(db,{query:q});
        return rp(dbConn.options)
            .then(d => {
                if(d.results.bindings[0].v.value == newVal){
                    errors.error = "SEAS-property already has the given value";
                    errors.statusCode = 400;
                    throw errors;
                }
                return;
            })
            .then(d => {
                const q =  `PREFIX seas: <https://w3id.org/seas/> \
                            PREFIX cdt: <http://w3id.org/lindt/custom_datatypes#> \
                            PREFIX prov: <http://www.w3.org/ns/prov#> \
                            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \
                            INSERT DATA {<${URI}> seas:evaluation [ seas:value "${newVal}"^^cdt:ucum ; prov:wasGeneratedAtTime "${dateTime}"^^xsd:dateTime ] . }`;
                console.log("Querying database: "+q);
                let dbConn = new StardogConn(db,{query:q});
                return rp(dbConn.options).then(d => {
                    if(d.boolean == false){
                        errors.error = "Could not create property";
                        errors.statusCode = 500;
                        throw errors;
                    }
                    return "Successfully updated property";
                });
            });
    }
    
    //Add a SEAS property value set
    addSeasPropertyValueSet(input: ISeasProperty){
        const db: string = input.req.params.db;
        const dateTime = moment().format();
        // Check if property is already defined
        const q =  `PREFIX seas: <https://w3id.org/seas/> \
                    SELECT ?p WHERE {<${input.resourceURI}> seas:${input.property} ?p}`;
        console.log("Querying database: "+q);
        let dbConn = new StardogConn(db,{query:q});
        return rp(dbConn.options)
            .then(result => {
                //If the property has already been defined (update)
                var newProp: boolean = true;
                var p: string = null;
                if(result.results.bindings.length != 0){
                    newProp = false;
                    p = result.results.bindings[0].p.value;
                }
                return {new: newProp, val: p };
            })
            .then(result => {
                //HANDLE NEW PROPERTY
                if(result.new == true){
                    //Create a URI for the resource
                    var propURI = new UriFunctions(input.req, "Property").newUri();
                    const q =  `PREFIX seas: <https://w3id.org/seas/> \
                                PREFIX cdt: <http://w3id.org/lindt/custom_datatypes#> \
                                PREFIX prov: <http://www.w3.org/ns/prov#> \
                                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> \
                                INSERT DATA { \
                                    <${input.resourceURI}> seas:${input.property} <${propURI}> . \
                                    <${propURI}> seas:evaluation [ seas:value "${input.value}"^^cdt:ucum ; prov:wasGeneratedAtTime "${dateTime}"^^xsd:dateTime ] . \
                                }`;
                    console.log("Querying database: "+q);
                    let dbConn = new StardogConn(db,{query:q});
                    return rp(dbConn.options).then(d => {
                        if(d.boolean == false){
                            errors.error = "Could not create property";
                            errors.statusCode = 500;
                            throw errors;
                        }
                        return result;
                    });
                } else {
                    return result;
                }
            })
            .then(result => {
                //HANDLE EXISTING PROPERTY UPDATE
                if(result.new == false){
                    return this.addPropertyEvaluation(db,result.val,input.value);
                } else {
                    return;
                }
            })
            .then(d => {
                return "Successfully added SEAS property";
            })
    }

}