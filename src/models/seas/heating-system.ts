import { Request } from "express";
import * as rp from "request-promise";
import { StardogConn } from "./../../config/stardog-connection";
var errors = require('request-promise/errors');

//Models
import { SeasModel } from "./_seas";

//Helper functions
import { UriFunctions } from "./../../helpers/uri-functions";

//Config
import { DbConfig } from './../../config/database';
import { AppConfig } from './../../config/app';

//Interfaces
import { ISeasProperty } from "./../../interfaces/seas-property";
import { ISeasFlowSystem } from "./../../interfaces/seas-flowsystem";
import { ISeasFlowPort } from "./../../interfaces/seas-flowsystem";

export class HeatingSystemModel extends SeasModel {

    //Create a new Heating System
    createHeatingSystem(req: Request){
        //Create resource
        const typeURI = "https://w3id.org/seas/HeatingSystem";
        return this.createNewResource(req,typeURI);
    }

    //List all heating systems in the project
    listHeatingSystems(req: Request){
        //Create resource
        const typeURI = "https://w3id.org/seas/HeatingSystem";
        return this.getResourcesOfType(req,typeURI);
    }

    //Delete a heating system and its properties
    //NB! SHOULD BE HANDLED IN ANOTHER WAY
    deleteHeatingSystem(req: Request){
        //Delete resource
        return this.deleteResource(req);
    }

    //Get all the properties defined for a specific heat consumer
    getHeatingSystemProperties(req: Request): any{
        //If querying for a specific property
        const property = req.query.property;
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const resourceURI: string = `https://${host}${req.originalUrl.split('?')[0]}`;
        console.log(property);
        if(property){
            switch(property) {
                case "fluidSupplyTemperature":
                    return this.checkIfExist(db,resourceURI)
                                .then(d => {
                                    return this.getSeasProperty(db,resourceURI,"fluidSupplyTemperature")
                                });
                case "fluidReturnTemperature":
                    return this.checkIfExist(db,resourceURI)
                                .then(d => {
                                    return this.getSeasProperty(db,resourceURI,"fluidReturnTemperature")
                                });
                default:
                    return new Promise ((resolve, reject) => resolve("Unknown property"));
            }
        }

        //Get properties
        return this.getResourceProperties(req);
    }

    //List all the properties of a heating system that are available in the ontology
    listHeatingSystemProperties(req: Request): any{
        //Define constants
        const db: string = req.params.db;
        const typeURI = "https://w3id.org/seas/HeatingSystem";
        
        //List properties
        return this.getAvailableProperties(db, typeURI);
    }

    /**
     * PROPERTIES
     * @method getTemperatureSet
     * @method setHeatConsumerProperty
     * @method setConsumerHeatOutput
     */
    
    setHeatingSystemProperty(req: Request): any{
        //Get property
        const property = req.query.property;
        switch(property) {
            case "temperatureSet":
                return this.setTemperatureSet(req);
            case "subFlowSystemOf":
                return this.makeSubFlowSystemOf(req);
            default:
                return new Promise ((resolve, reject) => resolve("Unknown property"));
        }
    }

    setTemperatureSet(req: Request): any{
        //Define constants
        const db: string = req.params.db;
        const host: string = req.headers.host.split(':')[0];
        const resourceURI: string = `https://${host}${req.originalUrl.split('?')[0]}`;
        const TF: string = req.body.t_flow;
        const TR: string = req.body.t_return;
        const sp: ISeasProperty = {req: req, resourceURI: resourceURI };
        
        //Check if resource exists
        return this.checkIfExist(db,resourceURI)
            .then(data => {
                //Check that input is correctly specified
                if(this.checkCDTUnit(TF, "Cel") && this.checkCDTUnit(TF, "Cel")){
                    return data;
                } else {
                    errors.error = "Please specify temperature set in degrees celcius. Example: '50.4 Cel' See http://unitsofmeasure.org/ucum.html#para-30 for further specifications.";
                    errors.statusCode = 400;
                    throw errors;
                }
            })
            .then(data => {
                //Attach fluid supply temperature to graph
                let sm = new SeasModel;
                sp.property = "fluidSupplyTemperature";
                sp.value = TF;
                return sm.addSeasPropertyValueSet(sp);
            })
            .then(data => {
                //Attach fluid return temperature to graph
                let sm = new SeasModel;
                sp.property = "fluidReturnTemperature";
                sp.value = TR;
                return sm.addSeasPropertyValueSet(sp);
            })
            .then(data => {
                return "Successfully attached temperature set."
            });
    }

}