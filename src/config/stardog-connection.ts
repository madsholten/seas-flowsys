import * as rp from "request-promise";
import { DbConfig } from './database';

//Interfaces
import { IQueryString } from "./../interfaces/qs";

export class StardogConn implements rp.RequestPromiseOptions {
    
    options: rp.OptionsWithUri;
    
    constructor(public dbParam: string, public qs: IQueryString){
        this.options = {
            method: 'GET',
            uri: `${DbConfig.stardog.host}:${DbConfig.stardog.port}/${dbParam}/query`,
            qs: qs,
            auth: {
                username: DbConfig.stardog.username,
                password: DbConfig.stardog.password
            },
            headers: {
                accept: "application/sparql-results+json"
            },
            json: true
        }
    }
}