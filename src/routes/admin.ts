import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./route";

//Models
import { BaseModel } from "./../models/model";
import { AdminModel } from "./../models/admin";

/**
 * / route
 *
 * @class Admin
 */
export class AdminRoute extends BaseRoute {

  /**
   * Create the routes.
   *
   * @class AdminRoute
   * @method create
   * @static
   */
  public static create(router: Router) {
    //log
    console.log("[AdminRoute::create] Creating admin route.");
    //List projects
    router.get("/admin/projects", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().listDBs(req, res, next);
    });
    //Wipe db
    router.delete("/:db/admin/wipe", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().wipeDB(req, res, next);
    });
    //add attach external ontology route
    router.post("/:db/admin/attach", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().attachOntology(req, res, next);
    });
    //add attach (reload) external ontology route
    router.put("/:db/admin/attach", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().reloadOntology(req, res, next);
    });
    //add detach external ontology route
    router.delete("/:db/admin/attach", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().detachOntology(req, res, next);
    });
    //add add namespace route
    router.post("/:db/admin/namespace", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().addNamespace(req, res, next);
    });
    //add remove namespace route
    router.delete("/:db/admin/namespace", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().removeNamespace(req, res, next);
    });
    //List rules
    router.get("/:db/admin/rules", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().listRules(req, res, next);
    });
    //Add rule
    router.post("/:db/admin/rule", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().addRule(req, res, next);
    });
    //Delete rule
    router.delete("/:db/admin/rule", (req: Request, res: Response, next: NextFunction) => {
      new AdminRoute().deleteRule(req, res, next);
    });
  }

  /**
   * Constructor
   *
   * @class AdminRoute
   * @constructor
   */
  constructor() {
    super();
  }

  /**
   * The admin route.
   *
   * @class AdminRoute
   * @method listDBs
   * @method wipeDB
   * @method attachOntology
   * @method reloadOntology
   * @method detachOntology
   * @method addNamespace
   * @method removeNamespace
   * @method listRules
   * @method addRule
   * @method deleteRule
   * @param req {Request} The express Request object.
   * @param res {Response} The express Response object.
   * @next {NextFunction} Execute the next method.
   */
  public listDBs(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.listDBs(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public wipeDB(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.wipeDB(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public attachOntology(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.attachOntology(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public reloadOntology(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.reloadOntology(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public detachOntology(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.detachOntology(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public addNamespace(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.addNamespace(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public removeNamespace(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.removeNamespace(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public listRules(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.listRules(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public addRule(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.addRule(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
  public deleteRule(req: Request, res: Response, next: NextFunction) {
    let am = new AdminModel()
    am.deleteRule(req)
      .then(function (data) {
        res.send(data);
        //this.render(req, res, data);
      })
      .catch(function (err) {
        res.send(err.error); 
      });
  }
}