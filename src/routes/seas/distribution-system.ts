import { NextFunction, Request, Response, Router } from "express";

//Routes
import { BaseRoute } from "./../route";

//Models
import { DistributionSystemModel } from "./../../models/seas/distribution-system";

/**
 * / route
 *
 * @class DistributionSystem
 */
export class DistributionSystemRoute extends BaseRoute {

  /**
   * Create the routes.
   *
   * @class DistributionSystemRoute
   * @method create
   * @static
   */
  public static create(router: Router) {    
    //log
    console.log("[DistributionSystemRoute::create] Creating Distribution System route.");
    
    //add hub route
    router.post("/:db/FlowHub", (req: Request, res: Response, next: NextFunction) => {
      new DistributionSystemRoute().createHub(req, res, next);
    });
    //get pipes
    router.get("/:db/Pipes", (req: Request, res: Response, next: NextFunction) => {
      new DistributionSystemRoute().getPipes(req, res, next);
    });

    router.post("/:db/FlowHub/:guid", (req: Request, res: Response, next: NextFunction) => {
      new DistributionSystemRoute().setHubProperty(req, res, next);
    });
  }

  /**
   * Constructor
   *
   * @class DistributionSystemRoute
   * @constructor
   */
  constructor() {
    super();
  }

  /**
  * The distribution system route.
  *
  * @class DistributionSystemRoute
  * @method createHub
  * @method getPipes
  * @method setHubProperty
  * @param req {Request} The express Request object.
  * @param res {Response} The express Response object.
  * @next {NextFunction} Execute the next method.
  */
  public createHub(req: Request, res: Response, next: NextFunction) {
    let dsm = new DistributionSystemModel();
    dsm.createHub(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public getPipes(req: Request, res: Response, next: NextFunction) {
    let dsm = new DistributionSystemModel();
    dsm.getPipes(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public setHubProperty(req: Request, res: Response, next: NextFunction) {
    let dsm = new DistributionSystemModel();
    dsm.setHubProperty(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  
}