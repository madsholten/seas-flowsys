import { NextFunction, Request, Response, Router } from "express";

//Routes
import { BaseRoute } from "./../route";

//Models
import { HeatingSystemModel } from "./../../models/seas/heating-system";

/**
 * / route
 *
 * @class HeatingSystem
 */
export class HeatingSystemRoute extends BaseRoute {

  /**
   * Create the routes.
   *
   * @class HeatingSystemRoute
   * @method create
   * @static
   */
  public static create(router: Router) {    
    //log
    console.log("[HeatingSystemRoute::create] Creating Heating System route.");
    
    //add heating system route
    router.post("/:db/HeatingSystem", (req: Request, res: Response, next: NextFunction) => {
      new HeatingSystemRoute().createHeatingSystem(req, res, next);
    });
    //list heating systems route
    router.get("/:db/HeatingSystems", (req: Request, res: Response, next: NextFunction) => {
      new HeatingSystemRoute().listHeatingSystems(req, res, next);
    });
    //delete heating system route
    router.delete("/:db/HeatingSystem/:guid", (req: Request, res: Response, next: NextFunction) => {
      new HeatingSystemRoute().deleteHeatingSystem(req, res, next);
    });
    //list available heating system properties route
    router.get("/:db/HeatingSystem/properties", (req: Request, res: Response, next: NextFunction) => {
      new HeatingSystemRoute().listHeatingSystemProperties(req, res, next);
    });
    //get heating system properties route
    router.get("/:db/HeatingSystem/:guid", (req: Request, res: Response, next: NextFunction) => {
      new HeatingSystemRoute().getHeatingSystemProperties(req, res, next);
    });
    //add property to heating system
    router.post("/:db/HeatingSystem/:guid", (req: Request, res: Response, next: NextFunction) => {
      new HeatingSystemRoute().setHeatingSystemProperty(req, res, next);
    });
  }

  /**
   * Constructor
   *
   * @class HeatingSystemRoute
   * @constructor
   */
  constructor() {
    super();
  }

  /**
   * The heating system route.
   *
   * @class HeatingSystemRoute
   * @method createHeatingSystem
   * @method listHeatingSystems
   * @method deleteHeatingSystem
   * @method getHeatingSystemProperties
   * @method listHeatingSystemProperties
   * @method setHeatingSystemProperty
   * @param req {Request} The express Request object.
   * @param res {Response} The express Response object.
   * @next {NextFunction} Execute the next method.
   */
  public createHeatingSystem(req: Request, res: Response, next: NextFunction) {
    //Create resource
    let hsm = new HeatingSystemModel();
    hsm.createHeatingSystem(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public listHeatingSystems(req: Request, res: Response, next: NextFunction) {
    //Create resource
    let hsm = new HeatingSystemModel();
    hsm.listHeatingSystems(req)
      .then(data =>  {
        res.send(data.results.bindings);
      })
      .catch(err => {
        next(err);
      });
  }
  public deleteHeatingSystem(req: Request, res: Response, next: NextFunction) {
    //Create resource
    let hsm = new HeatingSystemModel();
    hsm.deleteHeatingSystem(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public getHeatingSystemProperties(req: Request, res: Response, next: NextFunction) {
    let hsm = new HeatingSystemModel();
    hsm.getHeatingSystemProperties(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public listHeatingSystemProperties(req: Request, res: Response, next: NextFunction) {
    let hsm = new HeatingSystemModel();
    hsm.listHeatingSystemProperties(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public setHeatingSystemProperty(req: Request, res: Response, next: NextFunction) {
    let hsm = new HeatingSystemModel();
    hsm.setHeatingSystemProperty(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        if(err.statusCode && err.error) {
          next(err);
        }
      });
  }
}