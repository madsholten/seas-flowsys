import { NextFunction, Request, Response, Router } from "express";

//Routes
import { BaseRoute } from "./../route";

//Models
import { PropertyModel } from "./../../models/seas/property";

/**
 * / route
 *
 * @class Property
 */
export class PropertyRoute extends BaseRoute {

  /**
   * Create the routes.
   *
   * @class PropertyRoute
   * @method create
   * @static
   */
  public static create(router: Router) {    
    //log
    console.log("[PropertyRoute::create] Creating Property route.");
    
    //Get property
    router.get("/:db/Property/:guid", (req: Request, res: Response, next: NextFunction) => {
      new PropertyRoute().getProperty(req, res, next);
    });
  }

  /**
   * Constructor
   *
   * @class PropertyRoute
   * @constructor
   */
  constructor() {
    super();
  }

  /**
  * The property route.
  *
  * @class PropertyRoute
  * @method getProperty
  * @param req {Request} The express Request object.
  * @param res {Response} The express Response object.
  * @next {NextFunction} Execute the next method.
  */
  public getProperty(req: Request, res: Response, next: NextFunction) {
    let pm = new PropertyModel();;
    pm.getProperty(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  
}