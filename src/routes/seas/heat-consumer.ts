import { NextFunction, Request, Response, Router } from "express";

//Routes
import { BaseRoute } from "./../route";

//Models
import { HeatConsumerModel } from "./../../models/seas/heat-consumer";

/**
 * / route
 *
 * @class HeatConsumer
 */
export class HeatConsumerRoute extends BaseRoute {

  /**
   * Create the routes.
   *
   * @class HeatConsumerRoute
   * @method create
   * @static
   */
  public static create(router: Router) {    
    //log
    console.log("[HeatConsumerRoute::create] Creating Heat Consumer route.");
    
    //add heat consumer route
    router.post("/:db/HeatConsumer", (req: Request, res: Response, next: NextFunction) => {
      new HeatConsumerRoute().createHeatConsumer(req, res, next);
    });
    //list heat consumers route
    router.get("/:db/HeatConsumers", (req: Request, res: Response, next: NextFunction) => {
      new HeatConsumerRoute().listHeatConsumers(req, res, next);
    });
    //list available heat consumer properties route
    router.get("/:db/HeatConsumer/properties", (req: Request, res: Response, next: NextFunction) => {
      new HeatConsumerRoute().listHeatConsumerProperties(req, res, next);
    });
    //delete heat consumer route
    router.delete("/:db/HeatConsumer/:guid", (req: Request, res: Response, next: NextFunction) => {
      new HeatConsumerRoute().deleteHeatConsumer(req, res, next);
    });
    //get heat consumer properties route
    router.get("/:db/HeatConsumer/:guid", (req: Request, res: Response, next: NextFunction) => {
      new HeatConsumerRoute().getHeatConsumerProperties(req, res, next);
    });
    //add property to heat consumer
    router.post("/:db/HeatConsumer/:guid", (req: Request, res: Response, next: NextFunction) => {
      new HeatConsumerRoute().setHeatConsumerProperty(req, res, next);
    });
  }

  /**
   * Constructor
   *
   * @class HeatConsumerRoute
   * @constructor
   */
  constructor() {
    super();
  }

  /**
   * The heat consumer route.
   *
   * @class HeatConsumerRoute
   * @method createHeatConsumer
   * @method listHeatConsumers
   * @method deleteHeatConsumer
   * @method getHeatConsumerProperties
   * @method listHeatConsumerProperties
   * @method setHeatConsumerProperty
   * @param req {Request} The express Request object.
   * @param res {Response} The express Response object.
   * @next {NextFunction} Execute the next method.
   */
  public createHeatConsumer(req: Request, res: Response, next: NextFunction) {
    let hcm = new HeatConsumerModel();;
    hcm.createHeatConsumer(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public listHeatConsumers(req: Request, res: Response, next: NextFunction) {
    let hcm = new HeatConsumerModel();
    hcm.listHeatConsumers(req)
      .then(data =>  {
        res.send(data.results.bindings);
      })
      .catch(err => {
        next(err);
      });
  }
  public deleteHeatConsumer(req: Request, res: Response, next: NextFunction) {
    let hcm = new HeatConsumerModel();
    hcm.deleteHeatConsumer(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public getHeatConsumerProperties(req: Request, res: Response, next: NextFunction) {
    let hcm = new HeatConsumerModel();
    hcm.getHeatConsumerProperties(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public listHeatConsumerProperties(req: Request, res: Response, next: NextFunction) {
    let hcm = new HeatConsumerModel();
    hcm.listHeatConsumerProperties(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
  public setHeatConsumerProperty(req: Request, res: Response, next: NextFunction) {
    let hcm = new HeatConsumerModel();
    hcm.setHeatConsumerProperty(req)
      .then(data =>  {
        res.send(data);
      })
      .catch(err => {
        next(err);
      });
  }
}