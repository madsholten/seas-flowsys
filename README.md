# SEAS FlowSystems API

Install dependencies
`$ npm install`

Compile TS
`$ npm run grunt`

## Starting

To start the server run:

`$ npm start`

To run in dev:
`$ grunt nodemon`

## Database

For the API to work, please have a running Stardog database and specify the configuaration the following way:

1) Make a copy of src/config/_database.ts and name it database.ts
2) Change configuaration
3) Make a copy of src/config/_app.ts and name it app.ts
4) Change configuaration
5) If CORS is needed, also make your own copy of src/config/_cors.ts
6) Recompile

## Try it out!

1) Create a database for the project
2) Create a heat consumer by POST method to :url/:db/HeatConsumer. Body style:
`{"label": "Heater 1"}`
3) Attach output demand by POST method to :url/:db/HeatConsumer/:guid?property=heatOutput. Body style:
`{"output": "600 W"}`
4) Create a heating system by POST method to :url/:db/HeatingSystem. Body style:
`{"label": "Heating system 1"}`
5) Attach temperature set by POST method to :url/:db/HeatingSystem/:guid?property=temperatureSet. Body style:
`{"t_flow": "55 Cel", "t_return": "35 Cel"}`
6) Make the heat consumer a sub flow system of the heating system by POST method to :url/:db/HeatConsumer/:guid?property=subFlowSystemOf. Body style:
`{"super_system_URI": "https://localhost/seas/HeatingSystem/88d47871-bcf5-480a-831a-f3662b4a8149"}`
7) Attach an external ontology to enable reasoning by POST method to :url/:db/admin/attach. Body style:
`{"named_graph_name": "seas_sys", "graph_url": "w3id.org/seas/SystemOntology-1.0.ttl"}`
8) Attach a rule by POST method to :url/:db/admin/rule. Body style:
`{"label": "TEST RULE", "comment": "Just a test rule", "rule": "PREFIX seas:<https://w3id.org/seas/> IF { ?hc a seas:HeatConsumer } THEN { ?hc seas:test 'just testing' }"}`